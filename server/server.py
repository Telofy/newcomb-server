import json
import logging
from copy import copy
from json.decoder import JSONDecodeError

from aiohttp import web

from .models import UnknownFeatureError, prepared_models, trained_models

NON_FEATURE_COLUMNS = ["ResponseID", "features", "model"]

logger = logging.getLogger(__name__)


class Server:
    def _get_model(self, feature_names):
        try:
            return trained_models[feature_names]
        except UnknownFeatureError as exception:
            logger.error(exception.message)
            raise web.HTTPBadRequest(text=exception.message)
        except ValueError as exception:
            logger.error(exception.args[0])
            raise web.HTTPInternalServerError(text=exception.args[0])

    def _add_features(self, features):
        features = copy(features)
        if "payoff1" in features and "payoff2" in features:
            features["payoffRatio"] = features["payoff1"] - features["payoff2"]
        return features

    def _prune_features(self, features):
        if "features" in features:
            relevant = features["features"]
            features = {key: value for key, value in features.items() if key in relevant}
        features = {key: value for key, value in features.items() if key not in NON_FEATURE_COLUMNS}
        return features

    async def post(self, request):
        features = features_full = await request.json()

        features = self._add_features(features)
        features = self._prune_features(features)
        feature_names, feature_values = map(tuple, zip(*sorted(features.items())))

        if "model" in features_full:
            model = prepared_models[features_full["model"]]
        else:
            model = self._get_model(feature_names)

        [prediction] = model.predict([feature_values])

        response = {"Prediction": prediction}
        logger.debug("Request: %s Response: %s", json.dumps(features_full), json.dumps(response))
        return web.json_response(response)


app = web.Application()
app.add_routes([web.post("/", Server().post)])
