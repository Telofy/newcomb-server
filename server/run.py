import logging
from time import gmtime

from aiohttp import web

from .server import app
from .logger import DICT_CONFIG

logging.config.dictConfig(DICT_CONFIG)


if __name__ == "__main__":
    access_log_format = '%a "%r" %s %b "%{Referer}i" "%{User-Agent}i" (%D μs)'
    web.run_app(app, access_log_format=access_log_format)
