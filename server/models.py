import binascii
import pickle

import pandas as pd

from .newcomb.fit_and_save_rf import fit_test_rf

CSV_FILE = "newcomb-data.2019-06-15.csv"
PICKLE_FILE = "models/{}"
PAYOFF_FEATURES = set(["payoff1", "payoff2", "payoffRatio"])


class UnknownFeatureError(KeyError):
    def __init__(self, message, *args, **kwargs):
        super().__init__(message, *args, **kwargs)
        self.message = message


class PreparedModels(dict):
    def __init__(self, pickle_filename):
        self.pickle_filename = pickle_filename

    def __missing__(self, key):
        filename = self.pickle_filename.format(key)
        with open(filename, "rb") as pickle_file:
            model = pickle.load(pickle_file)
        self[key] = model
        return model


class TrainedModels(dict):
    def __init__(self, csv_filename, pickle_filename):
        self.csv_filename = csv_filename
        self.pickle_filename = pickle_filename
        self.data = self._read_data()

    def _read_data(self):
        data = pd.read_csv(self.csv_filename, na_values=["", " "])
        data["conscientiousness"] = (
            data["dependable_selfdisciplined"] - data["disorganized_careless"]
        )
        data["neuroticism"] = data["anxious"] - data["calm_emotionallystable"]
        data["CRTScore"] = data["crt_combined_recoded5"]
        return data

    def _fit_model(self, feature_names):
        feature_names = tuple(set(feature_names) - PAYOFF_FEATURES)
        unknown_features = tuple(set(feature_names) - set(self.data.columns))
        if unknown_features:
            raise UnknownFeatureError(f"Some features are not defined {unknown_features}")
        return fit_test_rf(self.data.copy(), feature_names)

    def _get_filename(self, feature_names):
        filename = "-".join(feature_names).encode("utf-8")  # Tuple -> binary string
        filename = f"{binascii.crc32(filename):#010x}.pickle"  # binary string -> hex checksum
        filename = self.pickle_filename.format(filename)  # hex checksum -> full path
        return filename

    def __missing__(self, key):
        filename = self._get_filename(key)
        try:
            with open(filename, "rb") as pickle_file:
                model = pickle.load(pickle_file)
        except IOError:
            model = self._fit_model(key)
            with open(filename, "wb") as pickle_file:
                pickle.dump(model, pickle_file)
        self[key] = model
        return model


prepared_models = PreparedModels(PICKLE_FILE)
trained_models = TrainedModels(CSV_FILE, PICKLE_FILE)
