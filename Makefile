NAME = newcomb-server
SHELL = /bin/bash
PYTHON = $(shell which python3.7)
VENVS = ~/.venvs
VENV = ${VENVS}/${NAME}
ACTIVATE = ${VENV}/bin/activate
LC_ALL = en_US.UTF-8
LC_CTYPE = en_US.UTF-8

default: install

${VENV}/bin/pip:
	${PYTHON} -m venv ${VENV}

pip-install:
	source ${ACTIVATE} && pip install poetry

install: ${VENV}/bin/pip pip-install
	source ${ACTIVATE} && poetry install

clean:
	# virtualenv
	rm -Rf ${VENV}
	find . -type f -name \*.pyc -delete
	find . -type d -name __pycache__ -delete